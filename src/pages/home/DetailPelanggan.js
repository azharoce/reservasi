import React,{Component} from 'react';
import { YellowBox,Button,View,Image,Text,StyleSheet ,AsyncStorage} from 'react-native';
import { Container,
  Header,
  Title,
  Content,
  Card,
  CardItem,
  Body,
  Left,
  Right} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { UltimateListView, UltimateRefreshView } from 'react-native-ultimate-listview';
import Dimensions from 'Dimensions';
const DEVICE_WIDTH = Dimensions.get('window').width;
import GlobalConfig from '../../GlobalConfig';
type Props={};
export default class DetailPelanggan extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      urlData:GlobalConfig.SERVERHOST+'pelanggan',
      HasilData : {},
    };
  }
  
  componentWillMount() {
  AsyncStorage.getItem('idPelanggan')
  .then((id)=>{
    this.loadDetail(id);
  });

  }
  
async loadDetail(id){
  await fetch(this.state.urlData+'/'+id,{
    method: 'get',
    headers: {'Content-Type': 'application/json'},
  })
  .then((response) => {
    return response.json();})
  .then((responseData) => {
    this.setState({
      HasilData: responseData
    });
  })
  .catch((error) => {
    alert(error);
  }).done();
  }
  async deleteData(id)
  {
  await fetch(GlobalConfig.SERVERHOST+'pelanggan/'+id, {
      method: "DELETE",
      headers:{'Content-Type': 'application/json'},
    }).then((response) => {
        return response.json();
      }).then((responseData) => {
    }).catch((error) => {

    }).done(() => {
       alert('success delete, please refresh data after delete data');
        this.props.navigation.navigate('Home')
    });
}

  render(){
    return (
      <Container style={styles.container}>
        <Content padder >
          <Card style={styles.mb}>
            <CardItem header>
              <Text>{this.state.HasilData.nama}</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                {this.state.HasilData.alamat}
                </Text>
              </Body>
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                {this.state.HasilData.umur}
                </Text>
              </Body>
            </CardItem>
            
            <CardItem footer>
              <Text>{this.state.HasilData.notelp}</Text>

              
            </CardItem>
            <View style={styles.login}>
          <Button
            title='edit'
            color='#00ace9'
            onPress={() =>  this.props.navigation.navigate('EditDetail')}
            style={{marginRigth:20}}
          />
          <Button
            title='delete'
            color='red'
            onPress={() => this.deleteData(this.state.HasilData._id)}
          />
        </View> 
          </Card>
        
        </Content>
       
        

      </Container>
    )
  }

}
const styles = StyleSheet.create({
  login: {
    width: '50%',
    margin: 18,
    alignItems:'center',
    padding:5,
    height:30,
    flex:1,
    flexDirection:'row'
  },
  MainContainer: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    justifyContent: 'center',
    alignItems: 'center',
  },

  cardViewStyle:{
    width: 350, 
    height: 250,
  },

  cardView_InsideText:{
    fontSize: 20, 
    color: '#000', 
    textAlign: 'center', 
    position:'absolute'
    // marginTop: 50    
  },
    container: {
      backgroundColor: "#FFF"
    },
    mb: {
      marginBottom: 15
    }
});
