import React, {Component} from 'react';
import {StyleSheet, KeyboardAvoidingView} from 'react-native';
import SignupInput from './SignupInput';
import SignupButton from './SignupButton';

type Props = {};
export default class FormSignup extends Component<Props> {
  render() {
    return (
      <KeyboardAvoidingView behavior='padding' style={styles.container}>
        <SignupInput
          icon='envelope'
          placeholder='Username'
          autoCapitalize={'none'}
          returnKeyType={'done'}
          autoCorrect={false}
          InputValue={this.state.username}
            onChangeText={(text) => this.setState({username: text})}     
        />
        <SignupInput
          icon='lock'
          placeholder='Password'
          returnKeyType={'done'}
          autoCapitalize={'none'}
          autoCorrect={false}
          InputValue={this.state.password}
          onChangeText={(text) => this.setState({password: text})}   
        />
        <SignupButton/>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  }
});