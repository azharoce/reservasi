import React, {Component} from 'react';
import {ScrollView,View, StyleSheet,Image,KeyboardAvoidingView,Button,ImageBackground} from 'react-native';
import SignupForm from './components/signup/SignupForm';
import SignupFooter from './components/signup/SignupFooter';
import SignupInput from './components/signup/SignupInput';
import SignupButton from './components/signup/SignupButton';
import LoginInput from '../login/components/LoginInput';
import GlobalConfig from '../../GlobalConfig';

type Props = {};
export default class Signup extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            jsonURL : GlobalConfig.SERVERHOST+'welcome/reg_data',
            items : [],
            isLoading : false,
            showPassword: true,
            isLoadingOverlay : true,
            status_login:null

        };
    }
    async SignupData(){
        var dataPost = {
            username:this.state.username,
            password:this.state.password,
            // username:"admin",
            // password:"admin"
            
        };
    //    alert(JSON.stringify(dataPost));
        await fetch(this.state.jsonURL, {
            method: "POST",
            headers:{'Content-Type': 'application/json'},
            body:JSON.stringify(dataPost)
          }).then((response) => {
              return response.json();
            }).then((responseData) => {
           alert(JSON.stringify(responseData));
        
              this.setState({status_login:responseData.status});
          }).catch((error) => {

          }).done(() => {
            if(this.state.status_login==200)
            {
            this.props.navigation.navigate('Login')
            }
          });
    }
    
  render() {
    return (
        <ImageBackground source={require('../../../assets/test.png')} style={{width:'100%',height:'100%',bottom:0,position:'absolute', flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#0a70b6',}} >
        <Image source={ require('../../../assets/logo.png')} style={{width:200,height:100,marginTop:50,marginBottom:30}}/>
        <KeyboardAvoidingView behavior='padding' style={styles.inputForm}>
        <LoginInput
          icon='envelope'
          placeholder='Username'
          autoCapitalize={'none'}
          returnKeyType={'done'}
          autoCorrect={false}
          InputValue={this.state.username}
          onChangeText={(text) => this.setState({username: text})}
        />
        <LoginInput
          icon='lock'
          placeholder='Password'
          returnKeyType={'done'}
          autoCapitalize={'none'}
          autoCorrect={false}
          InputValue={this.state.username}
          onChangeText={(text) => this.setState({password: text})}
        />
       
       <View style={styles.signup}>
          <Button
            title='Sign up'
            color='#09c856'
            // onPress={()=>alert('Signup')}
            onPress={() => this.SignupData()}

          />
        </View>
      </KeyboardAvoidingView>
            <SignupFooter/>  
        </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 2,
        backgroundColor: '#0a70b6',
    },
    inputForm: {
        flex: 1,
        alignItems: 'center',
      },
    view:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    signup: {
        width: 270,
        marginTop: 25
      }
});