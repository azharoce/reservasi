import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {StackNavigator} from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import Signup from './pages/users/Signup';
import Home from './pages/home/Home';
import Login from './pages/login/Login';
import DetailPelanggan from './pages/home/DetailPelanggan';
import TambahPelanggan from './pages/home/TambahPelanggan';
import EditDetail from './pages/home/EditDetail';
import Success_signup from './pages/users/components/Success_signup';

const RootStack = StackNavigator(
  {
    Login: {
      screen: Login,
      navigationOptions: () => ({
        header: null
      })
    },
    Signup: {
      screen: Signup,
      navigationOptions: () => ({
		header:null
      })
	},
	Home: {
		screen: Home,
		navigationOptions: () => ({
			header:null
		})
	},
	DetailPelanggan: {
		screen: DetailPelanggan,
		navigationOptions: () => ({
		  headerStyle: {
					backgroundColor: '#00ace9',
				  },
				  title: 'Detail Pelanggan',
				  headerTintColor: '#fff',
				  headerTitleStyle: {
					fontWeight: 'bold',
				  },
				  
		}),
	},
	TambahPelanggan: {
		screen: TambahPelanggan,
		navigationOptions: () => ({
		  headerStyle: {
					backgroundColor: '#00ace9',
				  },
				  title: 'Tambah Pelanggan',
				  headerTintColor: '#fff',
				  headerTitleStyle: {
					fontWeight: 'bold',
				  },
				  
		}),
	},
	EditDetail: {
		screen: EditDetail,
		navigationOptions: () => ({
		  headerStyle: {
					backgroundColor: '#00ace9',
				  },
				  title: 'Edit Data Pelanggan',
				  headerTintColor: '#fff',
				  headerTitleStyle: {
					fontWeight: 'bold',
				  },
				  
		}),
	},
	Success_signup: {
		screen: Success_signup,
		navigationOptions: () => ({
		  header:null
		})
	  },
  },
  {
    initialRouteName: 'Login',
  }
);

type Props = {};

export default class DrawerLayout extends Component<Props> {
	constructor(props)
	{
		super(props);
	}
	loginAct(token) {
		this.setState({
			tokenMember: token,
		});
		alert(JSON.stringify(token));
	}
	render() {
    return <RootStack />;
  }
}

const styles = StyleSheet.create({
  icon: {
    marginLeft: 13,
    color: '#ffffff'
	},
	iconRight: {
    marginRight: 13,
    color: '#ffffff'
  }
});
