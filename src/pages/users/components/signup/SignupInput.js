import React, {Component} from 'react';
import {StyleSheet, TextInput, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

type Props = {};
export default class SignupInput extends Component<Props> {
  render() {
    return (
      <View style={styles.section}>
        <TextInput
          style={styles.input}
          placeholder={this.props.placeholder}
          secureTextEntry={this.props.secureTextEntry}
          autoCorrect={this.props.autoCorrect}
          autoCapitalize={this.props.autoCapitalize}
          returnKeyType={this.props.returnKeyType}
          placeholderTextColor='#bdc3c7'
          // underlineColorAndroid='#bdc3c7'
          value={this.state.Inputvalue}
          onChangeText={(text) => this.props.onChangeText(text)}         
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    width: 270,
    height: 40,
    paddingLeft: 15,
    borderRadius:3,
    backgroundColor:'#ffffff',
    color: '#000000'
  },
  icon: {
    position: 'absolute',
    left: 5
  },
  section: {
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center'
  }
});