import React, { Component } from "react";
import { StyleSheet,Text ,Button,View} from 'react-native'
import {
  Container,
  Header,
  Title,
  Content,
  Body,
  Left,
  Right,
  Item,
  Input,
  Form
} from "native-base";
import GlobalConfig from '../../GlobalConfig';
import Icon from 'react-native-vector-icons/FontAwesome';

// https://jsonplaceholder.typicode.com/photos
type Props={};
export default class TambahPelanggan extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      urlData:GlobalConfig.SERVERHOST+'pelanggan',
      HasilData : {},
    };
  }
  
  async sendData(){
    var dataPost = {
        nama:this.state.nama,
        alamat:this.state.alamat,
        notelp:this.state.notelp,
        umur:this.state.umur,
        
    };
   alert('success added, please refresh data after added data');
    await fetch(this.state.urlData, {
        method: "POST",
        headers:{'Content-Type': 'application/json'},
        body:JSON.stringify(dataPost)
      }).then((response) => {
          return response.json();
        }).then((responseData) => {
    //    alert(JSON.stringify(responseData));
        // this.props.navigation.navigate('Home')
        //   this.setState({status_login:responseData.status});
      }).catch((error) => {

      }).done(() => {
        // if(this.state.status_login==200)
        // {
        this.props.navigation.navigate('Home')
        // }
      });
}
  render(){
    return (
        <Container style={styles.container}>
        <Content padder>
          <Form>
            <Item>
              <Icon active name="user" size={20}/>
              <Input placeholder="Full Name" value={this.state.nama} onChangeText={(text) => this.setState({nama: text})}/>
            </Item>

            <Item>
              <Icon active name="user-md" size={20}/>
              <Input placeholder="Umur"   value={this.state.umur} onChangeText={(text) => this.setState({umur: text})}/>
            </Item>

            <Item>
              <Icon active name="phone" size={20}/>
              <Input placeholder="Phone Number"   value={this.state.notelp} onChangeText={(text) => this.setState({notelp: text})}/>
            </Item>

            <Item>
              <Icon active name="map-marker" size={20}/>
              <Input placeholder="Address"  value={this.state.alamat} onChangeText={(text) => this.setState({alamat: text})} />
            </Item>
            <View style={styles.login}>
          <Button
            title='Login'
            color='#00ace9'
            
            onPress={() => this.sendData()}
          />
        </View> 
          </Form>
        </Content>
      </Container>
   
   )
  }

}

const styles = StyleSheet.create({
    container: {
      backgroundColor: "#FFF"
    },
    login: {
        width: '100%',
        marginTop: 18,
        height:100
      },
  });
  

