import React, {Component} from 'react';
import {StyleSheet, View, Button, Text, Spinner} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {withNavigation} from 'react-navigation';
import GlobalConfig from '../../../GlobalConfig';
class LoginButton extends React.Component {
  constructor(props) {
		super();
  }
  postJSONData() {
		this.props.sendData();
	}
  render() {
    return (
      <View>
        
        <View style={styles.login}>
          <Button
            title='Login'
            color='#09c856'
            onPress={() => this.postJSONData()}
            // onPress={()=>this.props.navigation.navigate('Home')}

          />
        </View> 
        
      </View>
    );
  }
}

export default withNavigation(LoginButton);

const styles = StyleSheet.create({
  login: {
    width: 270,
    marginTop: 18,
    height:100
  },
  forgot: {
    fontSize: 10,
    marginTop: 7,
    color: '#dddddd',
    textAlign: 'right'
  },
  text: {
    fontSize: 11,
    marginTop: 7,
    color: '#dddddd',
    textAlign: 'center'
  },
  signup: {
    fontSize: 12,
    marginTop: 10,
    color: '#eeeeee',
    textAlign: 'center',
    fontWeight: "bold"
  },
  connect: {
    marginTop: 7,
    flexDirection: 'row',
    justifyContent: 'center'
  }
});