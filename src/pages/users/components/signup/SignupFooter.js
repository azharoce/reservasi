import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {withNavigation} from 'react-navigation';

type Props = {};
class SignupFooter extends Component<Props> {
  render() {
    return (
        <View style={styles.bottom}>
            <Text style={styles.text}>
                Already have account?
                <Text style={styles.login} onPress={()=>this.props.navigation.navigate('Login')}>
                    {'  '}Login here
                </Text>
            </Text>
        </View>
    );
  }
}

export default withNavigation(SignupFooter);

const styles = StyleSheet.create({
  text: {
    fontSize: 11,
    marginTop: 7,
    color: '#dddddd',
    textAlign: 'center'
  },
  login: {
    fontSize: 12,
    marginTop: 10,
    color: '#eeeeee',
    textAlign: 'center',
    fontWeight: "bold"
  },
  bottom: {
    marginTop:20,
    marginBottom: 20
  }
});