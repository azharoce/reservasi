import React,{Component} from 'react';
import {StyleSheet ,AsyncStorage,Button,View} from 'react-native';
import {Container,Content,Item,Input,Form} from "native-base";
import Icon from 'react-native-vector-icons/FontAwesome';
import GlobalConfig from '../../GlobalConfig';
type Props={};
export default class EditDetail extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      urlData:GlobalConfig.SERVERHOST+'pelanggan',
      HasilData : {},
    };
  }
  componentWillMount() {
    AsyncStorage.getItem('idPelanggan')
      .then((id)=>{
      this.loadDetail(id);
    });
  }
  
loadDetail(id){
  fetch(this.state.urlData+'/'+id,{
    method: 'get',
    headers: {'Content-Type': 'application/json'},
  })
  .then((response) => {
    return response.json();})
  .then((responseData) => {
    this.setState({
      nama:responseData.nama,
      alamat:responseData.alamat,
      notelp:responseData.notelp,
      umur:responseData.umur,
      id:responseData._id,
    });
  })
  .catch((error) => {
    alert(error);
  }).done();
  }
  async sendData(id){
    var dataPost = {
        nama:this.state.nama,
        alamat:this.state.alamat,
        notelp:this.state.notelp,
        umur:this.state.umur,
        
    };
    await fetch(this.state.urlData+'/'+id, {
        method: "PUT",
        headers:{'Content-Type': 'application/json'},
        body:JSON.stringify(dataPost)
      }).then((response) => {
          return response.json();
        }).then((responseData) => {
      }).catch((error) => {

      }).done(() => {
       alert('success update, please refresh data after edit data');
        this.props.navigation.navigate('Home')
      });
}
  render(){
    return (
        <Container style={styles.container}>
        <Content padder>
          <Form>
            <Item>
              <Icon active name="user" size={20}/>
              <Input placeholder="Full Name" value={this.state.nama} onChangeText={(text) => this.setState({nama: text})}/>
            </Item>

            <Item>
              <Icon active name="user-md" size={20}/>
              <Input placeholder="Umur"   value={this.state.umur} onChangeText={(text) => this.setState({umur: text})}/>
            </Item>

            <Item>
              <Icon active name="phone" size={20}/>
              <Input placeholder="Phone Number" value={this.state.notelp}  onChangeText={(text) => this.setState({notelp: text})}/>
            </Item>

            <Item>
              <Icon active name="map-marker" size={20}/>
              <Input placeholder="Address" onChangeText={(text) => this.setState({alamat: text})} value={this.state.alamat} />
            </Item>
            <View style={styles.login}>
          <Button
            title='Update'
            color='#00ace9'
            
            onPress={() => this.sendData(this.state.id)}
          />
        </View> 
          </Form>
        </Content>
      </Container>
   
   )
  }

}

const styles = StyleSheet.create({
    container: {
      backgroundColor: "#FFF"
    },
    login: {
        width: '100%',
        marginTop: 18,
        height:100
      },
  });
  

