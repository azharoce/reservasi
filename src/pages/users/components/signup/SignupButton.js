import React, {Component} from 'react';
import {StyleSheet, View, Button} from 'react-native';
import {withNavigation} from 'react-navigation';

type Props = {};
class SignupButton extends Component<Props> {
  render() {
    return (
      <View>
        <View style={styles.signup}>
          <Button
            title='Sign up'
            color='#09c856'
            onPress={()=>alert('Signup')}
          />
        </View>
      </View>
    );
  }
}

export default withNavigation(SignupButton);

const styles = StyleSheet.create({
  signup: {
    width: 270,
    marginTop: 25
  }
});