import React, {Component} from 'react';
import {StyleSheet, KeyboardAvoidingView} from 'react-native';
import LoginInput from './LoginInput';
import GlobalConfig from '../../../GlobalConfig';

export default class LoginForm extends React.Component {
  constructor(props) {
		super();
			this.state = {
				jsonURL : GlobalConfig.SERVERHOST+'token/login',
				items : [],
				// email :'admin',
				// password :'admin',
				isLoading : false,
				showPassword: true,
        isLoadingOverlay : true,
				token:null
			}
    }
    postJSONData() {
      var dataPost = {
          username:this.state.username,
          password:this.state.password
        };
        alert(1);
        // alert(JSON.stringify(dataPost));
      // if (this.state.isLoading == false) {
      // this.setState({isLoading:true});
      // 	fetch(this.state.jsonURL, {
      // 		method: "post",
      // 		headers:{'Content-Type': 'application/json','Accept': 'application/json'},
      // 		body:JSON.stringify(dataPost)
      // 	}).then((response) => response.json()).then((responseData) => {
      // 		if(responseData.code==200 && responseData.data){
      // 			try {
      // 				this.setState({token:responseData.data});
      // 			} catch (error) {
      // 			}
      // 		}else{
      // 			Alert.alert("Username / Password didn't match")
      // 		}
      // 	}).done(() => {
      // 		this.setState({isLoading:false});
      // 		this.props.loginAct(this.state.token);
      // 	});
      // }
    }
  
  render() {
    return (
      <KeyboardAvoidingView behavior='padding' style={styles.container}>
        <View style={styles.section}>
        <Icon name={this.props.icon} size={20} color='#eeeeee' style={styles.icon}/>
        <TextInput
          style={styles.input}
          placeholder={this.props.placeholder}
          secureTextEntry={this.props.secureTextEntry}
          autoCorrect={this.props.autoCorrect}
          autoCapitalize={this.props.autoCapitalize}
          returnKeyType={this.props.returnKeyType}
          placeholderTextColor='#ffffff'
          InputValue={this.state.username}
          onChangeText={(text) => this.setState({username: text})}          
          underlineColorAndroid='#3498db'
        />
      </View>
        {/* <LoginInput
          icon='user'
          placeholder='Username'
          autoCapitalize={'none'}
          returnKeyType={'done'}
          autoCorrect={false}
          // InputValue={this.state.username}
          onChangeText={(text) => this.setState({username: text})}

        />
        <LoginInput
          icon='lock'
          placeholder='Password'
          returnKeyType={'done'}
          autoCapitalize={'none'}
          autoCorrect={false}
          secureTextEntry={true}
          value={this.state.password}
          onChangeText={(text) => this.setState({password: text})}
        /> */}
        {/* <LoginButton /> */}
        <Text style={styles.forgot} onPress={()=>alert('forgot')}>
          Forgot password ?
        </Text>
        <View style={styles.login}>
          <Button
            title='Login'
            color='#3498db'
            onPress={() => this.postJSONData()}
            // onPress={()=>this.props.navigation.navigate('Item')}
          />
        </View> 
        
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  input: {
    width: 270,
    height: 50,
    paddingLeft: 35,
    color: '#ffffff'
  },
  icon: {
    position: 'absolute',
    left: 5
  },
  section: {
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center'
  }
});