import React, {Component} from 'react';
import {withNavigation} from 'react-navigation';
import {View,Image, StyleSheet,KeyboardAvoidingView,Button} from 'react-native';


class Success_signup extends React.Component{
    render() {
    return (
        <View style={styles.container}>
			<Image source={ require('../../../../assets/logo.png')} style={{width:200,height:100,marginTop:50,marginBottom:30}}/>
        </View>
    );
  }
}
export default withNavigation(Success_signup);

const styles = StyleSheet.create({
    login: {
        width: 270,
        marginTop: 18,
        height:100
      },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#0a70b6',
    },
    input: {
        flex: 1,
        alignItems: 'center',
      }
});