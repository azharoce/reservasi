import React, {Component} from 'react';
import LoginInput from './components/LoginInput';
import LoginButton from './components/LoginButton';
import {withNavigation} from 'react-navigation';
import {View,Image, StyleSheet,KeyboardAvoidingView,Button,ImageBackground} from 'react-native';
import LoginFooter from './components/LoginFooter';
import GlobalConfig from '../../GlobalConfig';
class Login extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            jsonURL : GlobalConfig.SERVERHOST+'welcome/get_data',
            items : [],
            isLoading : false,
            showPassword: true,
            isLoadingOverlay : true,
            status_login:null

        };
    }
    async sendData(){
        // var dataPost = {
        //     // username:this.state.username,
        //     // password:this.state.password,
        //     username:"admin",
        //     password:"admin"
            
        // };
    // //    alert(JSON.stringify(this.state.jsonURL));
    //     await fetch('https://btpn-azharuddin.herokuapp.com/pelanggan', {
    //         method: "GET",
    //         headers:{'Content-Type': 'application/json'},
    //         // body:JSON.stringify(dataPost)
    //       }).then((response) => {
    //           return response.json();
    //         }).then((responseData) => {
    //        alert(JSON.stringify(responseData));
    //         //   this.setState({status_login:responseData.status});
    //       }).catch((error) => {

    //       }).done(() => {
    //         if(this.state.status_login==200)
    //         {
            this.props.navigation.navigate('Home')
            // }
        //   });
    }
    render() {
    return (
        
        <ImageBackground source={require('../../../assets/test.png')} style={{width:'100%',height:'100%',bottom:0,position:'absolute', flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',}} >
			<Image source={ require('../../../assets/logo.png')} style={{width:250,height:100,marginTop:50,marginBottom:30}}/>
             <KeyboardAvoidingView behavior='padding' style={styles.input}>
                <LoginInput
                placeholder='Username'
                autoCapitalize={'none'}
                onChangeText={(text) => this.setState({username: text})}
                returnKeyType={'done'}
                autoCorrect={false}
                />
                <LoginInput
                placeholder='Password'
				onChangeText={(text) => this.setState({password: text})}
                returnKeyType={'done'}
                autoCapitalize={'none'}
                autoCorrect={false}
                secureTextEntry={true}
                />
        <View style={styles.login}>
          <Button
            title='Login'
            color='#00ace9'
            onPress={() => this.sendData()}
          />
        </View> 
            </KeyboardAvoidingView>
          
            <LoginFooter/>
            
        </ImageBackground>
    );
  }
}
export default withNavigation(Login);

const styles = StyleSheet.create({
    login: {
        width: 270,
        marginTop: 18,
        height:100
      },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#0a70b6',
    },
    input: {
        flex: 1,
        alignItems: 'center',
      }
});