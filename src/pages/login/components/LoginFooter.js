import React, {Component} from 'react';
import {StyleSheet, View, Text,Image} from 'react-native';
import {withNavigation} from 'react-navigation';

type Props = {};
class LoginFooter extends Component<Props> {
  render() {
    return (
        <View style={styles.bottom}>
            <Text style={styles.text}>
                Don't have account?
                <Text style={styles.signup} onPress={()=>alert('belum woy')}>
                {'  '}Sign up here
                </Text>
            </Text>
        </View>
    );
  }
}

export default withNavigation(LoginFooter);

const styles = StyleSheet.create({
  text: {
    fontSize: 11,
    marginTop: 7,
    color: '#dddddd',
    textAlign: 'center'
  },
  signup: {
    fontSize: 12,
    marginTop: 10,
    color: '#eeeeee',
    textAlign: 'center',
    fontWeight: "bold"
  },
  bottom: {
    marginTop:20,
    marginBottom: 20
  }
});