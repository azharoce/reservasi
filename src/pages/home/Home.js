import React,{Component} from 'react';
import { YellowBox,Text,StyleSheet ,AsyncStorage,Image} from 'react-native';
import {Body, View,Left,Right,ListItem,Thumbnail} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { UltimateListView, UltimateRefreshView } from 'react-native-ultimate-listview';
import Dimensions from 'Dimensions';
const DEVICE_WIDTH = Dimensions.get('window').width;
import GlobalConfig from '../../GlobalConfig';
import { createStackNavigator,NavigationActions } from 'react-navigation'; // Version can be specified in package.json

type Props={};
var NavigationHome;
class TampilanHome extends Component<Props> {
  static navigationOptions = {
    tabBarVisible: false,
  }
    constructor(props) {
      super(props);
      this.state = {
        urlData : GlobalConfig.SERVERHOST+'pelanggan',
        DataList:[],
      }
      thatHome = this;
      YellowBox.ignoreWarnings(
    
        ['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader'
        
      ]);

  }

  onFetch = async(page = 0, startFetch, abortFetch) => {
      // alert((page*10));
      try {
          setTimeout(() => {
              fetch(this.state.urlData, {
                  method: "GET",
                  headers:{'Content-Type': 'application/json'},
              }).then((response) => response.json()).then((responseData) => {
                  startFetch(responseData,100);
                  // alert(responseData);
              }).catch((error) => {
                  alert("Error Connection"+error);
              }).done(() => {
          // abortFetch();

              });
    }, 1000);

      } catch (err) {
          abortFetch();
          console.log(err);
      }
  }
  cekDetail(id)
  {
    AsyncStorage.setItem('idPelanggan', id)
     .then(()=>{
      // alert(id);
      NavigationHome.navigate('DetailPelanggan');
       // this.props.navigation.navigate('Smile');
     });
  }
  _renderRowView = (item, index, separator) => {
      return(
        <ListItem style={{height:100,padding:0,marginLeft:5}}  avatar  key={index}  onPress={()=>{this.cekDetail(item._id)}}>
        <Left>
         <Image small source={{uri:'http://www.pendidikankarakter.com/wp-content/uploads/post-54-2.jpg'}}   style={{justifyContent: 'space-between',height: 60,width:60,borderRadius:2,resizeMode:'cover'}} />
        </Left>
        <Body style={{justifyContent: 'space-between',borderColor:'transparent'}}>
          <Text style={{fontWeight:'bold'}}>{item.nama}</Text>
          <Text style={{fontSize:10}}>{item.alamat}</Text>
           <Text style={{fontSize:10}}>{item.notelp}</Text>
        </Body>
      </ListItem>
      );
  };
    render(){
    return (
      <UltimateListView
      ref={(ref) => this._listView = ref}
      onFetch={this.onFetch}
      headerView={this.renderHeaderView}
      item={this._renderRowView}
      refreshableTitlePull="Pull To Refresh"
      refreshableMode="basic" //basic | advanced
      // refreshableMode="advanced" //basic or advanced
      numColumns={1}
      refreshable={true}
       ></UltimateListView>
    )
  }

}
const styles = StyleSheet.create({

  MainContainer: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    justifyContent: 'center',
    alignItems: 'center',
  },

  cardViewStyle:{
    width: 350, 
    height: 250,
  },

  cardView_InsideText:{
    fontSize: 20, 
    color: '#000', 
    textAlign: 'center', 
    position:'absolute'
    // marginTop: 50    
  }

});
const RootStack = createStackNavigator(
  {
    // DetailSinergi: HomeScreen,
    TampilanHome: {
      screen: TampilanHome,
        navigationOptions: () => ({
          headerStyle: {
            backgroundColor: '#00ace9',
            },
            title: 'Genius',
            headerTintColor: '#fff',
            headerTitleStyle: {
            fontWeight: 'bold',
            },
            headerLeft: <Icon name="bars" size={23} style={{marginLeft:13,color:'#ffffff'}} onPress={ () => alert('Belum cuy') } />,
            headerRight: <Icon name="plus" style={{marginRight:13,color:'#ffffff'}} size={23} onPress={ () => NavigationHome.navigate('TambahPelanggan') } />
        })
    },
  },
  {
    initialRouteName: 'TampilanHome',
   
  }
);

export default class Home extends Component<Props> {
  constructor(props){
    super(props)
    NavigationHome = props.navigation;
  }
  render() {
    return <RootStack />;
  }
}
